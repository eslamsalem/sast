# GitLab SAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/sast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/sast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)

GitLab SAST performs Static Application Security Testing on given source code.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by SAST, Dependency Scanning and their analyzers.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/code \
      --volume /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/gitlab-org/security-products/sast:${VERSION:-latest} /app/bin/run /code
    ```

    `VERSION` can be replaced with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-process) for more details.

1. The results will be displayed and also stored in `gl-sast-report.json`

**Why mounting the Docker socket?**

SAST acts as a container orchestrator: it will pull and run analyzers (Docker images) based on the languages and frameworks detected.

## Settings

See https://docs.gitlab.com/ee/user/application_security/sast/#available-variables.

## Development

### Build project

Go 1.13 or higher is highly recommended to build this SAST.

```sh
go build -o sast
```

### Run locally

To run the command locally and perform the scan on `/tmp/code`:

```sh
CI_PROJECT_DIR=/tmp/code ./sast
```

### Integration tests

To run the integration tests:

```sh
./test.sh
```

### Writing Custom Analyzers

#### Prerequisites

To write a custom analyzer that will integrate into the GitLab application
a minimum number of features are required.

For integration into the GitLab product itself, the license must be evaluated.

#### Checklist

##### Underlying tool

 - [ ] Has permissive software license
 - [ ] Headless execution (CLI tool)
 - [ ] Executable using GitLab Runner's [Linux or Windows Docker executor](https://docs.gitlab.com/runner/executors/README.html#docker-executor)
 - [ ] Language identification method (file extension, package file, etc)

##### Minimal vulnerability data

 - [ ] name
 - [ ] description (helpful but not mandatory)
 - [ ] type (unique value to avoid collisions with other occurrences)
 - [ ] file path
 - [ ] line number

## Supported languages, package managers and frameworks

See https://docs.gitlab.com/ee/user/application_security/sast/index.html#supported-languages-and-frameworks.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
