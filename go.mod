module gitlab.com/gitlab-org/security-products/sast/v2

require (
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/Microsoft/go-winio v0.4.11 // indirect
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/gogo/protobuf v1.1.1 // indirect
	github.com/gorilla/mux v1.7.0 // indirect
	github.com/sirupsen/logrus v1.3.0 // indirect
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/bandit/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/common/orchestrator/v2 v2.0.0-20200117152617-d63d543933da
	gitlab.com/gitlab-org/security-products/analyzers/common/table/v2 v2.0.0-20200117152617-d63d543933da
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.6.1
	gitlab.com/gitlab-org/security-products/analyzers/eslint/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/gosec/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/secrets/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/sobelow/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/tslint/v2 v2.0.0
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
	golang.org/x/time v0.0.0-20181108054448-85acf8d2951c // indirect
	google.golang.org/grpc v1.18.0 // indirect
	gotest.tools v2.2.0+incompatible // indirect
)

go 1.13
