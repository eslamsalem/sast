# GitLab SAST changelog

## v2.8.2
- Bump common to v2.6.1.

## v2.8.1
- Log when downloading, starting analyzers (!165)
- Suppress the progress message on pulling analyzer image (!165)

## v2.8.0
- Add Kubesec Kubernetes manifest analyzer.

## v2.7.2
- Bump common to v2.5.1, fix read permissions for nodejs-scan

## v2.7.1
- Bump `spotbugs` version for proper handling of comparison key uniqueness

## v2.7.0
- Add Salesforce Apex PMD analyzer.

## v2.6.0
- Removed deprecated `SAST_DEFAULT_ANALYZERS_ENABLED` environment variable from the README.
- Added `SAST_BANDIT_EXCLUDED_PATHS` environment variable to the README.

## v2.5.0
- Add `SAST_EXCLUDED_PATHS` option to exclude paths from report.

## v2.4.0
- Add Sobelow Elixir Phoenix analyzer.

## v2.3.0
- Add Secrets analyzer.

## v2.2.0
- Merge Find-Sec-Bugs analyzers into the spotbugs analyzer.

## v2.1.0
- Add TSLint security analyzer for Typescript

## v2.0.0
- Switch to new report syntax with `version` field.

## v1.12.0
- Add ESLint security analyzer for Javascript.

## v1.11.0
- Fix report when there are no vulnerabilities, should be an empty JSON array.

## v1.10.0
- Fix "out of memory" error when processing big repositories.

## v1.9.0
- Add `SAST_DEFAULT_ANALYZERS` env variable to explicitly list the official analyzers that are enabled.
- Deprecate `SAST_DEFAULT_ANALYZERS_ENABLED` because it's now redundant with `SAST_DEFAULT_ANALYZERS`.
- Add `SAST_ANALYZER_IMAGE_TAG` to set the Docker tag of the official analyzers (like `latest`).

## v1.8.0
- Set uid and gid when copying files for FSB Groovy.
- Use `Scanner` property instead of deprecated `Tool` when aggregating reports.

## v1.7.0
- Add Groovy analyzer.
- Rename GO AST SCANNER to gosec (https://gitlab.com/gitlab-org/gitlab-ee/issues/6999).

## v1.6.0
- Exit with code 4 when the directory given to scan is empty.

## v1.5.0
- Add [NodeJsScan](https://github.com/ajinabraham/NodeJsScan) analyzer to scan NodeJS applications.
- Add `SAST_ANALYZER_IMAGE_PREFIX` env variable to allow usage of custom Docker registry.
- Rename `DEFAULT_IMAGES` env variable into `SAST_DEFAULT_ANALYZERS_ENABLED`.
- Rename `PULL_IMAGES` env variable into `SAST_PULL_ANALYZER_IMAGES`.
- Rename `ANALYZER_IMAGES` env variable into `SAST_ANALYZER_IMAGES`.
- Add environment variables to set the timeouts.

## v1.4.0
- Set `SAST_BRAKEMAN_LEVEL` default value to `1` (Low) for consistency with other tools.
- Add [find-sec-bugs-sbt](https://find-sec-bugs.github.io/) analyzer to scan Scala sbt applications.
- Add [Security Code Scan](https://security-code-scan.github.io) analyzer to scan C# .NET applications.
- Enrich report with more data.

## v1.3.0
- Add `SAST_GO_AST_SCANNER_LEVEL` env variable to filter out warnings from Go AST Scanner below given confidence level.
- Rename `SAST_CONFIDENCE_LEVEL` env variable to `SAST_BRAKEMAN_LEVEL` (keep old one for compatibility).
- Fix issue matching by adding fingerprints to issues.
- Propagate environment variables to analyzers.
- Detect languages and frameworks in sub directories (with max depth=2).
- Ignore some directories.
- Add [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit) analyzer to scan PHP applications source code.
- Add [find-sec-bugs-graddle](https://find-sec-bugs.github.io/) analyzer to scan Java Graddle applications source code.

## v1.2.0
- Add [Flawfinder](https://www.dwheeler.com/flawfinder/) analyzer to scan C/C++ applications source code.
- Add [Go AST Scanner](https://github.com/GoASTScanner/gas) analyzer to scan Go applications source code.
- Display solutions to vulnerabilities if available.
- Wrap text in report output.
- **Breaking Change:** Extract Dependency Scanning from SAST into a
  [dedicated project](https://gitlab.com/gitlab-org/security-products/dependency-scanning). An update of your
  `.gitlab-ci.yml` is necessary to keep analyzing your dependencies, please
  check the [documentation](https://docs.gitlab.com/ee/ci/examples/dependency_scanning.html).

## v1.1.0
- Display a report of found vulnerabilities at the end.
- Add [find-sec-bugs](https://find-sec-bugs.github.io/) analyzer to scan Maven applications source code.

## v1.0.0
- Change versioning and release process.
- Add Gemnasium analyzer to check dependencies for various languages and package managers:
  * **Ruby**: rubygems.
  * **Javascript**: npm and yarn.
  * **PHP**: composer (with the _packagist_ registry).
  * **Python**: pip (with the _pypi_ registry). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
  * **Java**: maven (with the _central_ repository). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
- Add support for projects with multiple languages (execute all matching analyzers).
- Allow to disable analyzers that upload data to GitLab central server using `SAST_DISABLE_REMOTE_CHECKS` env variable.
- Fix priority for RetireJS issues.
- Fix generated file path for RetireJS issues.
- Dedupe issues coming from different tools and having the same CVE.
- Rename `CONFIDENCE_LEVEL` env variable to `SAST_CONFIDENCE_LEVEL` (shows a deprecation warning if old name is still used).
- Exit with code 1 and warns user when Docker-in-Docker is needed but missing.

**Warning:** an update of the `sast` job config in `.gitlab-ci.yml` is necessary to benefit from the latest features, please check the [documentation](https://docs.gitlab.com/ee/ci/examples/sast.html).

## v0.4.0
- Fix `CONFIDENCE_LEVEL` env variable ignoring string value.

## v0.3.0
- Allow pass `CONFIDENCE_LEVEL` env variable (works only with brakeman for now).

## v0.2.0
- Added Bandit, a python scanner.
- Sort issues in a report by priority.
